# brief_notes

A notes app to show off flutter UX polish

## Videos Screenshot Collage

# About Fred(Fredirck) Grott

Just using my andorid native experience to ramp up to flutter mobile development both 
contractual and remote possibly under the GrottUX biz name.

## Info

name: Fredrick Allan Grott

location: Greater Chicago Area(CST GMT -6)

remote: yes

skills: flutter, dart, android native, ios native, bash, docker, linux, unix, windows, macosx

## My Social Profiles

[Github](https://github.com/fredgrott)

[GitLab](https://gitlab.com/fred.grott)

[LinkedIn](https://www.linkedin.com/in/fredgrottstartupfluttermobileappdesigner/)

[Xing](https://www.xing.com/profile/Fred_Grott/cv)

[Dribbble](https://dribbble.com/FredGrott)

[BeHance](https://www.behance.net/gwsfredgrott)


# license

BSD Clause 2 Copyright 2020 Fredrick Allan Grott