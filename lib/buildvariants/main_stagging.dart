// BSD Clause 2 Copyright 2020 Fredrick Allan Grott

import '../dependencies_injector.dart';
import '../main.dart';

void main(){

  myAppInjector.get<AppBuildVariants>().name("stagging");
  myAppInjector.get<AppEndPoint>().appEndPoint("dummy");


  mainDelegate();
}