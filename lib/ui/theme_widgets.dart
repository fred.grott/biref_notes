// BSD Clause 2 Copyright 2020 Fredrick Allan Grott

import 'package:brief_notes/ui/theme_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_platform_widgets/flutter_platform_widgets.dart';

final ButtonThemeData myButtonTheme = ButtonThemeData(
  colorScheme: myLightColorScheme,
  splashColor: Colors.deepPurpleAccent[100],
);

final MaterialRaisedButtonData myMaterialRaisedButtonData = MaterialRaisedButtonData(

  color: Colors.purple,

);

final MaterialFlatButtonData myMaterialFlatButtonData = MaterialFlatButtonData(
  color: Colors.purple,
);
final CupertinoButtonData myCupertinoButtonData = CupertinoButtonData(


  color: primaryPurple,








);

final MaterialSwitchData myMaterialSwitchData = MaterialSwitchData(

  activeColor: Colors.purple,

);

final CupertinoSwitchData myCupertinoSwitchData = CupertinoSwitchData(
    activeColor: Colors.purple

);

final MaterialSliderData myMaterialSliderData = MaterialSliderData(

    activeColor: Colors.purple
);

final CupertinoSliderData myCupertinoSliderData = CupertinoSliderData(

    activeColor: Colors.purple
);

final MaterialIconButtonData myMaterialIconButtonData = MaterialIconButtonData(

);

final CupertinoIconButtonData myCupertinoIconButtonData = CupertinoIconButtonData(


  color: iconButtonPrimaryColor,




);

final MaterialProgressIndicatorData myMaterialProgressIndicatorData = MaterialProgressIndicatorData(
    backgroundColor: Colors.black12

);

final CupertinoProgressIndicatorData myCupertinoProgressIndicatorData = CupertinoProgressIndicatorData(

);

final MaterialAppBarData myMaterialAppBarData = MaterialAppBarData(






);
final CupertinoNavigationBarData myCupertinoNavigationBarData = CupertinoNavigationBarData(





);

final MaterialNavBarData myMaterialNavBarData = MaterialNavBarData(

);

final CupertinoTabBarData myCupertinotabBarData = CupertinoTabBarData(

);

final MaterialAlertDialogData myMaterialAlertDialogData = MaterialAlertDialogData(


);

final CupertinoAlertDialogData myCupertinoAlertDialogData = CupertinoAlertDialogData(




);

final MaterialDialogActionData myMaterialDialogActionData = MaterialDialogActionData(

);

final CupertinoDialogActionData myCupertinoDialogActionData = CupertinoDialogActionData(

);

final MaterialTabScaffoldData myMaterialTabScaffoldDate = MaterialTabScaffoldData(

);

final CupertinoTabScaffoldData myCupertinoTabScaffoldData = CupertinoTabScaffoldData(

);

final MaterialScaffoldData myMaterialScaffoldData = MaterialScaffoldData(

);

final CupertinoPageScaffoldData myCupertinoPageScaffoldData = CupertinoPageScaffoldData(

);

final MaterialTextFieldData myMaterialTextFieldData = MaterialTextFieldData(

);

final CupertinoTextFieldData myCupertinoTextFieldData = CupertinoTextFieldData(

);
