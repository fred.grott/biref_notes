import 'package:brief_notes/dependencies_injector.dart';
import 'package:brief_notes/main.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';










// remember acceptanace setup gets gitignored
// and thus actually is a build variant in principle
void main() {
  // This line enables the extension
  enableFlutterDriverExtension();
  myAppInjector.get<AppBuildVariants>().name("acceptance");
  myAppInjector.get<AppEndPoint>().appEndPoint("me.me.me");


  // Call the `main()` function of your app or call `runApp` with any widget you
  // are interested in testing.
  runApp(App());
}